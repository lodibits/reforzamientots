// diferencias
// type, class, interfaces
// son casi las mismas pero
// type y interfaces no existen en JS
// la interfaces es mas fácil de expandir que el tipo

// una interfaces le dice a typescript como va a lucir un objet
//la interfaces no puedes hacer nada solo pone reglas al objeto
interface Person {
  fullName: string;
  age: number;
  address: Address;
}

interface Address {
  country: string;
  homeNum: number;
}

export const ObjetosLiterales = () => {
  const person: Person = {
    fullName: 'lodi',
    age: 28,
    address: {
      country: 'mexico',
      homeNum: 13
    }
  };

  return (
    <div>
      <h3>objetos </h3>
      <code>
        <pre>{JSON.stringify(person, null, 2)}</pre>
      </code>
    </div>
  );
};

// JSON.stringify(person, null, 2)
//el valor null es un arreglo de un campo de esp ['n']
