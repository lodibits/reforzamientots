export const TiposBasicos = () => {
  const nombre: string = 'lodi';
  const edad: number = 28;

  const isActive: boolean = true;

  const poderes: string[] = ['Velocidad', 'Volar', 'Respirar'];

  poderes.push('Trueno');

  return (
    <div>
      <h1>TiposBasicos</h1>
      {nombre},{edad}
      {isActive ? ' activo' : 'no activo'}
      <br />
      {poderes.join(', ')}
    </div>
  );
};
