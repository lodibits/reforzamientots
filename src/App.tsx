// import {TiposBasicos} from './typescript/TiposBasicos';

// import { ObjetosLiterales } from './typescript/ObjetosLiterales';
import { Forms } from './components/Forms';

const App = () => {
  return (
    <div className='mt-2'>
      <h1>Hola type</h1>
      <hr />
      {/* <TiposBasicos/> */}
      {/* <ObjetosLiterales /> */}
      <Forms />
    </div>
  );
};

export default App;
