import { useState } from 'react';

// los hooks siempre usan use al inicio
// los genericos dependiendo de su argumento ese es el tipo
// T es el standar para el primer generico
export const useForm = <T extends Object>(formulario: T) => {
  const [state, setState] = useState(formulario);
  // tiene que ser una llave que este en T
  const onChange = (value: string, campo: keyof T) => {
    setState({
      ...state,
      // computando el valor es la propiedad que quiero establecer
      [campo]: value
    });
  };
  //exponer para que tengan acceso a el
  // si destructuring el formulario expones toda su estructura
  return {
    ...state,
    formulario: state,
    onChange
  };
};
