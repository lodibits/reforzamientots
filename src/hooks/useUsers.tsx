import { useEffect, useState, useRef } from 'react';
import { reqResApi } from '../api/reqRes';
import { ReqResListado, User } from '../interfaces/reqRes';
export const useUsers = () => {
  const [users, setUsers] = useState<User[]>([]);
  //el useRef es mejor usarlo
  //cuando no hay cambios en el html
  //no disparara el rendering

  const pageRef = useRef(1);
  // para tomar su valor usa el x.curren

  useEffect(() => {
    //llamado al api
    cargarUsuarios();
  }, []);

  const cargarUsuarios = async () => {
    const resp = await reqResApi.get<ReqResListado>('/users', {
      params: {
        page: pageRef.current
      }
    });
    if (resp.data.data.length > 0) {
      setUsers(resp.data.data);
    } else {
      pageRef.current--;
      alert('no more data');
    }
  };

  const pageAfter = () => {
    pageRef.current++;
    cargarUsuarios();
  };

  const pageBefore = () => {
    if (pageRef.current > 1) {
      pageRef.current--;
      cargarUsuarios();
    }
  };

  return { users, pageAfter, pageBefore };
};
