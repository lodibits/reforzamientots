import { useUsers } from '../hooks/useUsers';
import { User } from '../interfaces/reqRes';

export const Usuarios = () => {
  const { users, pageAfter, pageBefore } = useUsers();
  const renderItem = (user: User) => {
    return (
      <tr key={user.id.toString()}>
        <td>
          <img
            style={{ width: 34, borderRadius: 100 }}
            src={user.avatar}
            alt={user.first_name}
          />
        </td>
        <td>{user.first_name}</td>
        <td>{user.email}</td>
      </tr>
    );
  };
  return (
    <>
      <h3>Usuarios</h3>
      <table className='table'>
        <thead>
          <tr>
            <th>Avatar</th>
            <th>Nombre</th>
            <th>email</th>
          </tr>
        </thead>
        <tbody>{users.map(renderItem)}</tbody>
      </table>
      <button className='btn btn-primary' onClick={pageAfter}>
        anteriores
      </button>
      <button className='btn btn-primary mx-3' onClick={pageBefore}>
        siguientes
      </button>
    </>
  );
};
