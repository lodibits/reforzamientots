import { useState } from 'react';
// Valores typescript generics su salida es de ese valor
export const Contador = () => {
  const [value, setValue] = useState<number>(0);
  const acumular = (num: number) => {
    setValue(value + num);
  };

  return (
    <>
      <h2>
        contador <small>{value}</small>
      </h2>
      <button className='btn btn-primary mx-1' onClick={() => acumular(1)}>
        +1
      </button>
      <button className='btn btn-primary' onClick={() => acumular(-1)}>
        -1
      </button>
    </>
  );
};
