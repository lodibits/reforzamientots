import { useForm } from '../hooks/useForm';
export const Forms = () => {
  const { formulario, email, password, onChange } = useForm({
    email: 'test@test',
    password: '123'
  });
  return (
    <>
      <h3>formularios</h3>
      <input
        type='text'
        className='from-control'
        placeholder='Email'
        value={email}
        onChange={({ target }) => onChange(target.value, 'email')}
      />
      <input
        type='text'
        className='from-control mt-2 mb-2  '
        placeholder='password'
        value={password}
        onChange={({ target }) => onChange(target.value, 'password')}
      />
      <code>
        <pre>{JSON.stringify(formulario, null, 2)}</pre>
      </code>
    </>
  );
};
