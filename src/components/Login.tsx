import { useEffect, useReducer } from 'react';

interface AuthState {
  validando: boolean;
  token: string | null;
  username: string;
  name: string;
}

// 1.- Estado inicial
const initialState: AuthState = {
  validando: true,
  token: null,
  username: '',
  name: ''
};

type LoginActionPayload = {
  username: string;
  name: string;
};
//type son objetos planos y no generan code en js
// definir como un objeto
//type nombre y payload lo que quieres mandar
type AuthAction =
  | { type: 'logout' }
  | { type: 'login'; payload: LoginActionPayload };

// 2.- reducer es un func
// necesita tener un state y una action
// action modifica al state
const authReducer = (state: AuthState, action: AuthAction): AuthState => {
  switch (action.type) {
    case 'logout':
      return {
        name: '',
        token: null,
        username: '',
        validando: false
      };
    case 'login':
      const { name, username } = action.payload;
      return {
        name,
        token: '39393k39',
        username,
        validando: false
      };
    default:
      return state;
  }
};
export const Login = () => {
  //reducer func para retornar un nuevo estado
  //initialState es el estado inicial
  //state
  //dispatch func para disparar acciones
  //como el state es un objeto se puede destructuring
  const [{ validando, token, name }, dispatch] = useReducer(
    authReducer,
    initialState
  );

  useEffect(() => {
    setTimeout(() => {
      dispatch({ type: 'logout' });
    }, 1500);
  }, []);

  const login = () => {
    dispatch({
      type: 'login',
      payload: {
        name: 'lodo',
        username: 'lodi'
      }
    });
  };

  const logout = () => {
    dispatch({ type: 'logout' });
  };

  if (validando) {
    return (
      <>
        <h3>Login</h3>
        <div className='alert alert-info'>Validando</div>
      </>
    );
  }

  return (
    <div>
      <h3>Login</h3>

      {token ? (
        <div className='alert alert-success'>Autenticado como {name}</div>
      ) : (
        <div className='alert alert-danger'>No autentificado</div>
      )}

      {token ? (
        <button className='btn btn-danger' onClick={logout}>
          Logout
        </button>
      ) : (
        <button
          className='btn btn-primary mx-3
        '
          onClick={login}
        >
          Login
        </button>
      )}
    </div>
  );
};
